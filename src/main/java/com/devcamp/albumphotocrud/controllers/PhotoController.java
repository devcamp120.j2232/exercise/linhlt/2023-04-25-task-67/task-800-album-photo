package com.devcamp.albumphotocrud.controllers;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.*;
import org.springframework.web.bind.annotation.*;

import com.devcamp.albumphotocrud.model.CAlbum;
import com.devcamp.albumphotocrud.model.CPhoto;
import com.devcamp.albumphotocrud.repository.IAlbumRepository;
import com.devcamp.albumphotocrud.repository.IPhotoRepository;

import java.util.*;

import javax.validation.Valid;

@RestController
@CrossOrigin
@RequestMapping("/photo")
public class PhotoController {
    @Autowired
    IPhotoRepository photoRepository;
    @GetMapping("/all")
    public ResponseEntity<List<CPhoto>> getAllPhoto(){
        try {
            List<CPhoto> photoList = new ArrayList<>();
            photoRepository.findAll().forEach(photoList::add);
            return new ResponseEntity<>(photoList, HttpStatus.OK);
        } catch (Exception e) {
            return new ResponseEntity<>(null, HttpStatus.NOT_FOUND);
        }
    }
    
    @GetMapping("detail/{id}")
    public ResponseEntity<CPhoto> getPhotoById(@PathVariable long id){
        try {
            CPhoto photo = photoRepository.findById(id);
            if (photo != null){
                return new ResponseEntity<>(photo, HttpStatus.OK);
            }
            else return new ResponseEntity<>(null, HttpStatus.NOT_FOUND);
        } catch (Exception e) {
            return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @Autowired
    IAlbumRepository albumRepository;
    @PostMapping("/create/{id}")
    public ResponseEntity<Object> createPhoto(@PathVariable("id") long id, @Valid @RequestBody CPhoto pPhoto){
        try {
            CAlbum album = albumRepository.findById(id);
            if (album != null){
                CPhoto newPhoto = new CPhoto();
                newPhoto.setCode(pPhoto.getCode());
                newPhoto.setName(pPhoto.getName());
                newPhoto.setTitle(pPhoto.getTitle());
                newPhoto.setUrl(pPhoto.getUrl());
                newPhoto.setCreated(new Date());
                newPhoto.setAlbum(album);
                CPhoto _photo = photoRepository.save(newPhoto);
                return new ResponseEntity<>(_photo, HttpStatus.CREATED);
            }
            else return new ResponseEntity<>(null, HttpStatus.NOT_FOUND);
        } catch (Exception e) {
            System.out.println("+++++++++++++++++++++::::: "+e.getCause().getCause().getMessage());
			return ResponseEntity.unprocessableEntity().body("Failed to Create specified Photo: "+e.getCause().getCause().getMessage());
        }
    }

    @PutMapping("/update/{id}")
    public ResponseEntity<CPhoto> updatePhotoById(@PathVariable("id") long id, @Valid @RequestBody CPhoto pPhoto){
        try {
            CPhoto photo = photoRepository.findById(id);
            if (photo != null){
                photo.setCode(pPhoto.getCode());
                photo.setName(pPhoto.getName());
                photo.setTitle(pPhoto.getTitle());
                photo.setUrl(pPhoto.getUrl());
                
                return new ResponseEntity<>(photoRepository.save(photo), HttpStatus.OK);
            } else {
                return new ResponseEntity<>(HttpStatus.NOT_FOUND);
            }
        } catch (Exception e) {
            System.out.println(e);
			return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @DeleteMapping("/delete/{id}")
	public ResponseEntity<CPhoto> deletePhotoById(@PathVariable("id") long id) {
		try {
			photoRepository.deleteById(id);
			return new ResponseEntity<>(HttpStatus.NO_CONTENT);
		} catch (Exception e) {
			System.out.println(e);
			return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
		}
	}
}
